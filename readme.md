# Convenio Colectivo de Transporte Sanitario de Enfermos y Accidentados en Ambulancias de Gipuzkoa 2008-2013

![Alt text](https://bytebucket.org/tarteka/convenio-ambulancias-gipuzkoa/raw/f65ef2db0bc9e8d613a62f9e3a7c91b95e15ac82/convenio.png)

+ El siguiente Convenio Colectivo es una **transcripción del original del B.O.G.**
+ Incorpora **un índice para facilitar la búsqueda** de información.
+ Se proporciona en dos versiones:
    - En **formato LaTex** para poder ser ediado.
    - En **formato pdf** para imprimir en hojas DinA4.

### Descarga directamente el archivo PDF:
[Convenio en PDF](https://bitbucket.org/tarteka/convenio-ambulancias-gipuzkoa/raw/02b01cc42c02ab8cbfe8245ff1b0d8dd8c4f1ff7/convenio-latex.pdf)

### Para crear el documento en formato cuadernillo

+ Instalar `LaTex` y `latex-pdfpages`
+ Descargar el archivo `convenio-latex.pdf`
+ Crear un documento `tex` nuevo. Ejemplo: `cuadernillo.tex`
+ Pegar el siguiente código, y compilarlo:

```
\documentclass[a4paper]{book}
\usepackage{pdfpages}
\begin{document}
\includepdf[pages=-,booklet=true, landscape]{convenio-latex.pdf}
\end{document}
```

El resultado será, un pdf nuevo, llamado `cuadernillo.pdf`, listo para imprimir.

### Descarga directamente el archivo PDF en formato _cuadernillo_:

[Convenio en formato cuardenillo PDF](https://bitbucket.org/tarteka/convenio-ambulancias-gipuzkoa/raw/da37cdfc90c9275c55c91512ed361ed3578701bf/convenio-cuadernillo.pdf)

### Aviso
Los siguientes archivos no tienen ninguna validez legal. Los únicos 100% fiables, son los expuestos en el [Boletín Oficial de Gipuzkoa](https://ssl4.gipuzkoa.net/boletin/asp/ViewRoot.asp?Action=Html&Item=1&X=1202184738).

Si encuentras algun error, eres libre de modificarlo o de avisarme.