# Convenio Colectivo de Transporte Sanitario de Enfermos y Accidentados en Ambulancias de Gipuzkoa 2008-2013

![Alt text](https://gitlab.com/tarteka/convenio-ambulancias-gipuzkoa/raw/c134167187e40b0189c2ebd047c042bbb6629875/convenio.png)

+ El siguiente Convenio Colectivo es una **transcripción del original del B.O.G.**
+ Incorpora **un índice para facilitar la búsqueda** de información.
+ Se proporciona en dos versiones:
    - En **formato LaTex** para poder ser ediado.
    - En **formato pdf** para imprimir en hojas DinA4.

### Descarga directamente el archivo PDF:
[Convenio en PDF](https://gitlab.com/tarteka/convenio-ambulancias-gipuzkoa/raw/master/convenio-latex.pdf)

### Para crear el documento en formato cuadernillo

+ Instalar `LaTex` y `latex-pdfpages`
+ Descargar el archivo `convenio-latex.pdf`
+ Crear un documento `tex` nuevo. Ejemplo: `cuadernillo.tex`
+ Pegar el siguiente código, y compilarlo:

```
\documentclass[a4paper]{book}
\usepackage{pdfpages}
\begin{document}
\includepdf[pages=-,booklet=true, landscape]{convenio-latex.pdf}
\end{document}
```

El resultado será, un pdf nuevo, llamado `cuadernillo.pdf`, listo para imprimir.

### Descarga directamente el archivo PDF en formato _cuadernillo_:

[Convenio en formato cuardenillo PDF](https://gitlab.com/tarteka/convenio-ambulancias-gipuzkoa/raw/master/convenio-cuadernillo.pdf)

### Aviso
_Los siguientes archivos no tienen ninguna validez legal. Los únicos 100% fiables, son los expuestos en el [Boletín Oficial de Gipuzkoa](https://ssl4.gipuzkoa.net/boletin/asp/ViewRoot.asp?Action=Html&Item=1&X=1202184738)._

_Si encuentras algun error, eres libre de modificarlo o de avisarme._